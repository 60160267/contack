/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;

/**
 *
 * @author informatics
 */
public class contac {

    static ArrayList<Data> list = new ArrayList<Data>();
    static Data person = new Data();
    private static String username;
    private static String password;
    private static int typeAskBack;

    public static void main(String[] args) {
        if (login()) {
            showMenu();
        } else {

        }

    }

    private static void showlogin() {
        System.out.println("Login");
    }

    private static void inputlogin() {
        Scanner key = new Scanner(System.in);
        System.out.print("Username : ");
        username = key.nextLine();
        System.out.print("Password : ");
        password = key.nextLine();
    }

    private static boolean login() {
        while(true){
            if (inputChooseLogin()) {
                showlogin();
                inputlogin();
                if (!Data.isUserPassCorrect(username, password)) {
                    showErrorLogin();
                } else {
                    return true;
                }
            } else {
                askBack();
                if(checkAskBack()){
                   return false ;
                }
            }
        }
    }

    private static void showErrorLogin() {
        System.out.println("Error : Username or Password incorrect");
    }

    private static boolean inputChooseLogin() {
        Scanner key = new Scanner(System.in);
        while (true) {
            showChooseLogin();
            int type = key.nextInt();
            if (type == 1 || type == 2) {
                if (type == 1) {
                    return true;
                } else {
                    showExit();
                    return false;
                }
            } else {
                ErrorChooseLogin();
            }
        }
    }

    private static void showExit() {
        System.out.println("Exit!!");
    }

    private static void ErrorChooseLogin() {
        System.out.println("Error : Please input number 1 or 2");
    }

    private static void showChooseLogin() {
        System.out.println("1. Login");
        System.out.println("2. Exit");
        System.out.print("Please input number (1,2) : ");
    }

    private static void askBack() {
        System.out.println("Do you want to exit program ?  \n 0.Yes \n 1.No");
    }

    private static boolean checkAskBack() {
        Scanner key = new Scanner(System.in);
        while (true) {
            typeAskBack = key.nextInt();
            if (typeAskBack == 0 || typeAskBack == 1) {
                if (typeAskBack == 0) {
                    showExit();
                    return true ;
                } else {
                    return false ;
                }
            } else {
                showErrorcheckAskBack();
            }
        }
    }

    private static void showErrorcheckAskBack() {
        System.out.println("Error : Please input number 0 or 1");
        askBack();
    }

    private static void showMenu() {
        System.out.println("1. addUser \n2. editUser \n3.showUser \n4. logout \nPlease input number (1-4) :");
    }

}
