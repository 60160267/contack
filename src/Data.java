/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author informatics
 */
public class Data {
    static ArrayList<Person> list = new ArrayList<Person>();
    
    public static void load() {
      list.add(new Person("Admin","Admin","admin","admin","0956414523",64,176));
      list.add(new Person("Test1","Test1","Test1","Test1","0956423623",60,170));
      list.add(new Person("Test2","Test2","Test2","Test2","0986423623",70,179));
      list.add(new Person("Test3","Test3","Test3","Test3","0965341523",60,179));
      list.add(new Person("Test4","Test4","Test4","Test4","0912453269",75,180));
    }
    
    static boolean isUserPassCorrect (String username , String password){
        load();
        for (Person p : list){
            if (username.equals(p.getusername())){
                if (password.equals(p.getPassword())){
                    return true;
                }
            }
        }
        return false;
    }
}
