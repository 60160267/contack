/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
class Person {
    
    String name;
    String surname ; 
    String username ; 
    String password ; 
    String tel ; 
    int weight ; 
    int height ;
    
    Person (String name, String surname , String username , String password , String tel , int weight , int height){
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    Person() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    String getName() {
        return name;
    }
    String getSurname() {
        return surname;
    }
    String getusername() {
        return username;
    }
    String getPassword() {
        return password;
    }
    String getTel() {
        return tel;
    }
    int getWeight() {
        return weight;
    }
    int getHeight() {
        return height;
    }
    
    void setName(String name){
        this.name = name;
    }
    void setSurname(String surname){
        this.surname = surname;
    }
    void setUsername(String username){
        this.username = username;
    }
    void setPassword(String password){
        this.password = password;
    }
    void setTel(String tel){
        this.tel = tel;
    }
    void setWeight(int weight){
        this.weight = weight;
    }
    void setHeight(int height){
        this.height = height;
    }
        
}
